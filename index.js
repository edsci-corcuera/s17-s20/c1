const email = document.querySelector("#email");
const formSubmit = document.querySelector("#form-submit");

formSubmit.addEventListener("click", function() {
	alert("Thank your for sending an email. I will reach you out to your at your email " + email.value + " regarding your concern. Have a great day!");
})